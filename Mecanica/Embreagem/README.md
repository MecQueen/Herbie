# Especificações do sistema de embreagem:  
#### ~ (todas as medidas em milimetros) ~

#### 1.Embreagem_M

>- Raio menor:30
>- Raio maior(apoio do rolamento):33
>- Altura total:65
>- Altura do cilindro inferior(apoio do rolamento):25
>- Altura das partes de encaixe: 15 (aprox)

>>##### Engrenagem interior(cavidade interior da peça):
- Dentes:10
- Angulo de força:20
- módulo:16/10 = 1,6 
_(o módulo é definido por "diametro/número de dentes")_
- diametro:16 

#### 1.1Eixo da Embreagem_M

>##### Eixo (Engrenagem exterior)
- Dentes:10
- Angulo de força:20
- módulo:13/10= 1,3 
_(o módulo é definido por "diametro/número de dentes")_
- diametro:13
- Altura:80

>>##### Cilindro de encaixe acoplado ao eixo:
- Altura:30
- Raio:30

_(Mola meramente ilustrativa)_ 

---  


#### 2.Embreagem_F

>- Raio:30
- Altura:35
- Altura das partes de encaixe: 15 (aprox)

>>##### Eixo:
- raio:4
- altura:140 (aprox)

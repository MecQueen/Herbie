# Especificações da caixa de marchas:  
#### ~ (todas as medidas em milimetros)
#### Razão de transmissão expressa como _(Motor:Diferencial)_

## Características Gerais
> - Ângulo de Força:15°
- Módulo:0,9

> #### Engrenagens transmissoras
- Possuem espaço interno para um rolamento 608 (8x22x7)

> #### Engrenagens intermediarias
- Diâmetro para eixo:4

## 1ª Marcha
#### Razão de transmissão 2,9:1 

> #### Engrenagem intermediaria
- Dentes:20

> #### Engrenagem transmissora
- Dentes:58


## 2ª Marcha
#### Razão de transmissão 2:1
> #### Engrenagem intermediaria
- Dentes:26 

> #### Engrenagem transmissora
- Dentes:52


## 3ª Marcha
#### Razão de transmissão 1,455:1(aprox)
> #### Engrenagem intermediaria
- Dentes:33  

> #### Engrenagem transmissora
- Dentes:48


## 5ª Marcha
#### Razão de transmissão 0,857:1(aprox)    
> #### Engrenagem intermediaria
- Dentes:42   

> #### Engrenagem transmissora
- Dentes:36

## Eixo transmissor
>- Diâmetro:8

## Eixo Intermediario
>- Diâmetro:4
# Regras de Desenvolvimento para Eletrônica

  * Devem ser usadas no Eagle, as regras de design disponibilizadas em Eletrônica/DRU.
  * Para esquemáticos, deve ser montado o circuito dentro de uma grid com cabeçalho e título.
  * Não devem haver ligações elétricas se cruzando em esquemáticos.
  * Devem ser feitas áreas de distribuição para as linhas de alimentação, uma em cada camada.
  * **Sempre** que possível, a placa deve ser de face simples. Se necessário, utilize jumpers.


# Regras de Desenvolvimento Mecânico

  * Todas as medidas devem estar grafadas em mm, exceto quando maiores que 100 mm, devendo ser, então, grafadas em cm.
  * A espessura mínima de peças deve ser de 0,3mm.
  * Para todo encaixe, deve haver uma folga mínima de 0,2mm para garantir o encaixe suave das peças.
  * O parafuso a ser utilizado para montagens deve Allen de 2mm

# Regras de Desenvolvimento de Software

## Variáveis      

  * Se globais, devem ser declaradas em Camel Case com a primeira letra maiúscula conforme o exemplo:
```
    NumeroDecimal VolumeDoCilindro = PI*Raio*Raio;
```

  * Se variáveis, devem ser declaradas em Camel Case com a primeira letra minúscula conforme o exemplo:
```
    Numero raioDoCirculo = 3;
```

  * Se constantes, devem ser declaradas como macros.
  * Os nomes devem ter comprimento menor ou igual a 15 caracteres e DEVEM SER SUGESTIVOS!

## Macros

  * Devem ser declaradas com todas as letras maiúsculas conforme o exemplo:

```
    definir PI 3.14
```  

## Funções

   * Se tem acesso geral, devem ser declaradas em Camel Case com a primeira letra minúscula conforme exemplo:
```
    Numero soma(x, y){
        ...
    }
```

   * Se tem acesso restrito, devem ser declaradas em Camel Case precedidos por '_' conforme exemplo:
```
    Numero _mult( x, y){
        /* usada apenas dentro da funcao elevar */
        ...
    }
```

## Formatação

   * Se's, Enquanto's e Para's com apenas um comando devem ser declarados sem chaves com tabulação simples conforme o exemplo:
```
    se(x==y)
        Pino.ligar(x);
```

   * Após a declaração de funções, deve haver uma linha em branco antes de outras funções ou variáveis conforme o exemplo:     
```c
        ...
    }
    
    Numero soma(){
        ...
    } 
```

   * A tabulação deve ser feita com 4 espaços.
   * É preferível a declaração de todas as variáveis de um mesmo tipo juntos, uma por linha precedidas por meia tabulação, no início do escopo, conforme exemplo:
```
    Numero main(){
        Numero x = 0,
          y = 0;
        ...
    } 
```

   * A declaração de vetores bidimensionais deve ser feita com linhas separadas para cada vetor da segunda dimensão, com tabulação para alinhar as colunas, conforme exemplo:
```
    Numero vetor[2][2] = {{0, 0},
                       {0, 0}};
```

   * Operadores devem ser precedidos e sucedidos de um, e apenas um, espaço em branco.
   * Parênteses, Chaves e Colchetes não devem ser precedidos, nem sucedidos de espaços em branco.
   * Comentários de documentação dos argumentos e retorno da função devem preceder a declaração da função, com dois asteriscos na abertura. Não devem ter texto na primeira linha e devem seguir o modelo:
```
    /**
     * Descricao
     *
     * @retorno Numero:
     *      O maior dos operandos ou 0 se forem iguais
     *
     * @param x1:
     *     primeiro operando
     * @param x2:
     *     segundo operando
     *
     */ 
```
     

# Reportando bugs
   * Sempre utilize o template disponibilizado!
   * Leia o link [Como escrever um bom relatório de bug](http://www.softwaretestinghelp.com/how-to-write-good-bug-report/)
    

# Solicitando novas funcionalidades
   1. Crie um issue descrevendo a nova funcionalidade
   2. Crie uma branch para desenvolver a nova funcionalidade
   3. Crie um pull request marcado com WIP enquanto desenvolve sua funcionalidade
    

# Fechando pull requests
   1. **Sempre que possível** rode testes unitários e disponibilize os resultados     
   1. Marque alguém como revisor
   2. Aguarde o revisor rodar os testes novamente e marcar o PR como aprovado
   3. Aguarde ao menos 3 :+1: (`:+1:`) para realizar o fechamento
   